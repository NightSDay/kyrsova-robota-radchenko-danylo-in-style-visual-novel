﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinterStory
{
    public partial class Form1 : Form
    {
        public int goodside = 1;
        public int badside = 1;
        public Form1()
        {
            InitializeComponent();
            buttonBackSlide.Visible = false;
            buttonNextSlide.Visible = false;
            labelfirstslide.Visible = false;
            labelbeforeFirstChoise.Visible = false;
            buttonGoStaight.Visible = false;
            buttonGoAnotherWay.Visible = false;
            labelAfterFirstCoise.Visible = false;
            labelSecondAfterFirstChoise.Visible = false;
            labelThirdAfterFirstChoise.Visible = false;
            buttonGood2.Visible = false;
            buttonGood3.Visible = false;
            labelArgue.Visible = false;
            labelIgnore.Visible = false;
            buttonGoToManeMenu.Visible = false;
            labelForBadone.Visible = false;
            labelBadTwo.Visible = false;
            buttonStayHome.Visible = false;
            buttonGoSomewhere.Visible = false;
            labelGoSomewhere.Visible = false;
            labelStayHome.Visible = false;


        }
        private void buttonInfo_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Ця гра,жанру візуальна новелла, показує невелику але насичену історію одного хлопця,якому" +
                " так не щастило з коханням,але все ж таки йому випав шанс знайти його...і від" +
                " наших дій буде залежити,будуть його любити чи ні..", "Коротка інформація", MessageBoxButtons.OK);
        }

        private void buttonBeginGame_Click(object sender, EventArgs e)
        {
            buttonInfo.Visible = false;
            labelGreetings.Visible = false;
            BackgroundImage = null;
            this.BackgroundImage = Image.FromFile(@"H:\Курсова робота\WinterStory\pictures\firstslide\1600x900Back.jpg");
            buttonBeginGame.Visible = false;
            buttonBackSlide.Visible = false;
            buttonNextSlide.Visible = true;
            labelfirstslide.Visible = true;


        }

        private void buttonBackSlide_Click(object sender, EventArgs e)
        {
            goodside--;
            if(goodside==1)
            {
                BackgroundImage = null;
                this.BackgroundImage = Image.FromFile(@"H:\Курсова робота\WinterStory\pictures\firstslide\1600x900Back.jpg");
                buttonBackSlide.Visible = false;
                buttonNextSlide.Visible = true;
                labelfirstslide.Visible = true;
            }
            if (goodside == 2)
            {
                BackgroundImage = null;
                this.BackgroundImage = Image.FromFile(@"H:\Курсова робота\WinterStory\pictures\firstslide\1600x900Back.jpg");
                buttonGoStaight.Visible = false;
                buttonGoAnotherWay.Visible = false;
                labelfirstslide.Visible = false;
                labelbeforeFirstChoise.Visible = true;
                buttonNextSlide.Visible = true;

            }
            if (goodside == 3)
            {
                buttonGoStaight.Visible = true;
                buttonGoAnotherWay.Visible = true;
                labelbeforeFirstChoise.Visible = false;
                this.BackgroundImage = Image.FromFile(@"H:\Курсова робота\WinterStory\pictures\choises\choisse1.png");
                labelAfterFirstCoise.Visible = false;
                labelSecondAfterFirstChoise.Visible = false;
                labelThirdAfterFirstChoise.Visible = false;
                buttonBackSlide.Visible = true;
                buttonNextSlide.Visible = false;

            }
            if (goodside == 4)
            {
                buttonGoStaight.Visible = true;
                buttonGoAnotherWay.Visible = true;
                BackgroundImage = null;
                this.BackgroundImage = Image.FromFile(@"H:\Курсова робота\WinterStory\pictures\choises\choisse1.png");
                labelAfterFirstCoise.Visible = false;
                labelSecondAfterFirstChoise.Visible = false;
                buttonBackSlide.Visible = false;
                buttonNextSlide.Visible = false;



            }
            if (goodside == 5)
            {
                labelSecondAfterFirstChoise.Visible = false;
                labelThirdAfterFirstChoise.Visible = false;
            }
            if (badside == 99)
            {
                buttonBackSlide.Visible = false;
                buttonNextSlide.Visible = true;
                labelForBadone.Visible = true;
                labelBadTwo.Visible = false;
            }
            if (badside == 100)
            {
                buttonBackSlide.Visible = true;
                labelForBadone.Visible = false;
                labelBadTwo.Visible = true;
            }
            badside--;
        }

        private void buttonNextSlide_Click(object sender, EventArgs e)
        {
            if(goodside==1)
            {
                BackgroundImage = null;
                this.BackgroundImage = Image.FromFile(@"H:\Курсова робота\WinterStory\pictures\firstslide\1600x900Back.jpg");
                buttonGoStaight.Visible = false;
                buttonGoAnotherWay.Visible = false;
                labelfirstslide.Visible = false;
                labelbeforeFirstChoise.Visible = true;
                buttonBackSlide.Visible = true;

            }
            if (goodside==2)
            { buttonGoStaight.Visible = true;
            buttonGoAnotherWay.Visible = true;
            buttonBackSlide.Visible = false;
            buttonNextSlide.Visible = false;
            labelfirstslide.Visible = false;
            labelbeforeFirstChoise.Visible = false;
                this.BackgroundImage = Image.FromFile(@"H:\Курсова робота\WinterStory\pictures\choises\choisse1.png");
            }
           if(goodside==4)
            {
                labelAfterFirstCoise.Visible = false;
                labelSecondAfterFirstChoise.Visible = true;
                buttonBackSlide.Visible = true;


            }
            if (goodside==5)
            {
                buttonBackSlide.Visible = true;
                labelSecondAfterFirstChoise.Visible = false;
                labelThirdAfterFirstChoise.Visible = true;
            }
           if(goodside==6)
            {
                labelThirdAfterFirstChoise.Visible = false;
                buttonBackSlide.Visible = false;
                buttonNextSlide.Visible = false;
                buttonGood2.Visible = true;
                buttonGood3.Visible = true;

            }
            //
            goodside++;
            //
            if(badside==101)
            {          
                labelForBadone.Visible = false;
                labelBadTwo.Visible = true;
                buttonBackSlide.Visible = false;
                buttonNextSlide.Visible = true;
                labelSecondAfterFirstChoise.Visible = false;

            }
            if (badside==102)
            {
                buttonBackSlide.Visible = false;
                buttonNextSlide.Visible = false;
                labelBadTwo.Visible = false;
                buttonGoSomewhere.Visible = true;
                buttonStayHome.Visible = true;
                labelSecondAfterFirstChoise.Visible = false;

            }

            badside++;

        }
        private void buttonGoStaight_Click(object sender, EventArgs e)
        {
            buttonBackSlide.Visible = false;
            buttonNextSlide.Visible = true;
            buttonGoStaight.Visible = false;
            buttonGoAnotherWay.Visible = false;
            BackgroundImage = null;
            this.BackgroundImage = Image.FromFile(@"H:\Курсова робота\WinterStory\pictures\afterfirstchoise.jpg");
            labelAfterFirstCoise.Visible = true;
            goodside++;
        }

        private void buttonGoAnotherWay_Click(object sender, EventArgs e)
        {
            buttonGoStaight.Visible = false;
            buttonGoAnotherWay.Visible = false;
            labelfirstslide.Visible = false;
            BackgroundImage = null;
            this.BackgroundImage = Image.FromFile(@"H:\Курсова робота\WinterStory\pictures\badchoise.png");
            labelForBadone.Visible = true;
            badside+=98;
            buttonNextSlide.Visible = true;
            buttonBackSlide.Visible = false;

        }

        private void buttonGood2_Click(object sender, EventArgs e)
        {
            labelArgue.Visible = true;
            buttonGoToManeMenu.Visible = true;
            buttonGood3.Visible = false;
            buttonGood2.Visible = false;
            buttonBackSlide.Visible = false;

        }

        private void buttonGood3_Click(object sender, EventArgs e)
        {
            labelIgnore.Visible = true;
            buttonGoToManeMenu.Visible = true;
            buttonGood3.Visible = false;
            buttonGood2.Visible = false;
            buttonBackSlide.Visible = false;

        }

        private void buttonCloseProgram_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void buttonGoToManeMenu_Click(object sender, EventArgs e)
        {
            buttonInfo.Visible = true;
            labelGreetings.Visible = true;
            BackgroundImage = null;
            this.BackgroundImage = Image.FromFile(@"H:\Курсова робота\WinterStory\pictures\mainpic.jpg");
            buttonBeginGame.Visible = true;
            buttonBackSlide.Visible = false;
            buttonNextSlide.Visible = false;
            labelArgue.Visible = false;
            labelIgnore.Visible = false;
            goodside = 0;
            badside = 0;
            buttonGoToManeMenu.Visible = false;

        }

        private void buttonStayHome_Click(object sender, EventArgs e)
        {
            buttonBackSlide.Visible = false;
            buttonNextSlide.Visible = false;
            buttonGoToManeMenu.Visible = true;
            buttonStayHome.Visible = false;
            buttonGoSomewhere.Visible = false;
            labelStayHome.Visible = true;
        }

        private void buttonGoSomewhere_Click(object sender, EventArgs e)
        {
            buttonBackSlide.Visible = false;
            buttonNextSlide.Visible = false;
            buttonGoToManeMenu.Visible = true;
            buttonStayHome.Visible = false;
            buttonGoSomewhere.Visible = false;
            labelGoSomewhere.Visible = true;
        }
    }
}
