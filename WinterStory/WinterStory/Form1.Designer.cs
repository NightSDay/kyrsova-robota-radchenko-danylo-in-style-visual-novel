﻿
namespace WinterStory
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.buttonBeginGame = new System.Windows.Forms.Button();
            this.buttonInfo = new System.Windows.Forms.Button();
            this.labelGreetings = new System.Windows.Forms.Label();
            this.buttonNextSlide = new System.Windows.Forms.Button();
            this.buttonBackSlide = new System.Windows.Forms.Button();
            this.labelfirstslide = new System.Windows.Forms.Label();
            this.buttonGoAnotherWay = new System.Windows.Forms.Button();
            this.buttonGoStaight = new System.Windows.Forms.Button();
            this.labelbeforeFirstChoise = new System.Windows.Forms.Label();
            this.labelAfterFirstCoise = new System.Windows.Forms.Label();
            this.labelSecondAfterFirstChoise = new System.Windows.Forms.Label();
            this.labelThirdAfterFirstChoise = new System.Windows.Forms.Label();
            this.buttonGood2 = new System.Windows.Forms.Button();
            this.buttonGood3 = new System.Windows.Forms.Button();
            this.labelArgue = new System.Windows.Forms.Label();
            this.labelIgnore = new System.Windows.Forms.Label();
            this.buttonCloseProgram = new System.Windows.Forms.Button();
            this.buttonGoToManeMenu = new System.Windows.Forms.Button();
            this.labelForBadone = new System.Windows.Forms.Label();
            this.labelBadTwo = new System.Windows.Forms.Label();
            this.buttonStayHome = new System.Windows.Forms.Button();
            this.buttonGoSomewhere = new System.Windows.Forms.Button();
            this.labelGoSomewhere = new System.Windows.Forms.Label();
            this.labelStayHome = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonBeginGame
            // 
            this.buttonBeginGame.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.buttonBeginGame.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonBeginGame.BackgroundImage")));
            this.buttonBeginGame.Font = new System.Drawing.Font("Monotype Corsiva", 20.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonBeginGame.Location = new System.Drawing.Point(584, 359);
            this.buttonBeginGame.Name = "buttonBeginGame";
            this.buttonBeginGame.Size = new System.Drawing.Size(355, 73);
            this.buttonBeginGame.TabIndex = 0;
            this.buttonBeginGame.Text = "Почати поринання в історію";
            this.buttonBeginGame.UseVisualStyleBackColor = true;
            this.buttonBeginGame.Click += new System.EventHandler(this.buttonBeginGame_Click);
            // 
            // buttonInfo
            // 
            this.buttonInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonInfo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonInfo.BackgroundImage")));
            this.buttonInfo.Font = new System.Drawing.Font("Poor Richard", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonInfo.Location = new System.Drawing.Point(12, 639);
            this.buttonInfo.Name = "buttonInfo";
            this.buttonInfo.Size = new System.Drawing.Size(328, 41);
            this.buttonInfo.TabIndex = 1;
            this.buttonInfo.Text = "Коротка Інформація щодо проекту(Short Info about project)";
            this.buttonInfo.UseVisualStyleBackColor = true;
            this.buttonInfo.Click += new System.EventHandler(this.buttonInfo_Click);
            // 
            // labelGreetings
            // 
            this.labelGreetings.AllowDrop = true;
            this.labelGreetings.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labelGreetings.BackColor = System.Drawing.Color.LightSeaGreen;
            this.labelGreetings.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelGreetings.Location = new System.Drawing.Point(1305, 335);
            this.labelGreetings.Name = "labelGreetings";
            this.labelGreetings.Size = new System.Drawing.Size(154, 127);
            this.labelGreetings.TabIndex = 7;
            this.labelGreetings.Text = "Приємної вам гри і отримуйте насолодження від коротенької але насиченої історії";
            this.labelGreetings.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonNextSlide
            // 
            this.buttonNextSlide.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonNextSlide.BackColor = System.Drawing.Color.Aquamarine;
            this.buttonNextSlide.Location = new System.Drawing.Point(1222, 626);
            this.buttonNextSlide.Name = "buttonNextSlide";
            this.buttonNextSlide.Size = new System.Drawing.Size(177, 42);
            this.buttonNextSlide.TabIndex = 8;
            this.buttonNextSlide.Text = "Листати далі";
            this.buttonNextSlide.UseVisualStyleBackColor = false;
            this.buttonNextSlide.Click += new System.EventHandler(this.buttonNextSlide_Click);
            // 
            // buttonBackSlide
            // 
            this.buttonBackSlide.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBackSlide.BackColor = System.Drawing.Color.Aquamarine;
            this.buttonBackSlide.Location = new System.Drawing.Point(101, 627);
            this.buttonBackSlide.Name = "buttonBackSlide";
            this.buttonBackSlide.Size = new System.Drawing.Size(177, 41);
            this.buttonBackSlide.TabIndex = 9;
            this.buttonBackSlide.Text = "Повернути назад";
            this.buttonBackSlide.UseVisualStyleBackColor = false;
            this.buttonBackSlide.Click += new System.EventHandler(this.buttonBackSlide_Click);
            // 
            // labelfirstslide
            // 
            this.labelfirstslide.AllowDrop = true;
            this.labelfirstslide.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labelfirstslide.BackColor = System.Drawing.Color.LightSeaGreen;
            this.labelfirstslide.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelfirstslide.Location = new System.Drawing.Point(431, 626);
            this.labelfirstslide.Name = "labelfirstslide";
            this.labelfirstslide.Size = new System.Drawing.Size(637, 91);
            this.labelfirstslide.TabIndex = 10;
            this.labelfirstslide.Text = "Я:Це сон чи ні? тяжко зрозуміти і тяжко сфокусуватися.. Голова розколюється а-а-а" +
    "х.. Голос: йди до мене, йди на мій голос..";
            this.labelfirstslide.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonGoAnotherWay
            // 
            this.buttonGoAnotherWay.Location = new System.Drawing.Point(476, 456);
            this.buttonGoAnotherWay.Name = "buttonGoAnotherWay";
            this.buttonGoAnotherWay.Size = new System.Drawing.Size(110, 38);
            this.buttonGoAnotherWay.TabIndex = 11;
            this.buttonGoAnotherWay.Text = "Зійти з дороги";
            this.buttonGoAnotherWay.UseVisualStyleBackColor = true;
            this.buttonGoAnotherWay.Click += new System.EventHandler(this.buttonGoAnotherWay_Click);
            // 
            // buttonGoStaight
            // 
            this.buttonGoStaight.Location = new System.Drawing.Point(936, 456);
            this.buttonGoStaight.Name = "buttonGoStaight";
            this.buttonGoStaight.Size = new System.Drawing.Size(108, 38);
            this.buttonGoStaight.TabIndex = 12;
            this.buttonGoStaight.Text = "Піти по дорозі";
            this.buttonGoStaight.UseVisualStyleBackColor = true;
            this.buttonGoStaight.Click += new System.EventHandler(this.buttonGoStaight_Click);
            // 
            // labelbeforeFirstChoise
            // 
            this.labelbeforeFirstChoise.AllowDrop = true;
            this.labelbeforeFirstChoise.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labelbeforeFirstChoise.BackColor = System.Drawing.Color.LightSeaGreen;
            this.labelbeforeFirstChoise.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelbeforeFirstChoise.Location = new System.Drawing.Point(431, 626);
            this.labelbeforeFirstChoise.Name = "labelbeforeFirstChoise";
            this.labelbeforeFirstChoise.Size = new System.Drawing.Size(637, 91);
            this.labelbeforeFirstChoise.TabIndex = 13;
            this.labelbeforeFirstChoise.Text = "Я:і так кожен раз коли сниться ця дорога, поки не виберу сон не кінчиться..Навіть" +
    " самогубство тут не допоможе";
            this.labelbeforeFirstChoise.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelAfterFirstCoise
            // 
            this.labelAfterFirstCoise.AllowDrop = true;
            this.labelAfterFirstCoise.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labelAfterFirstCoise.BackColor = System.Drawing.Color.LightSeaGreen;
            this.labelAfterFirstCoise.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelAfterFirstCoise.Location = new System.Drawing.Point(431, 626);
            this.labelAfterFirstCoise.Name = "labelAfterFirstCoise";
            this.labelAfterFirstCoise.Size = new System.Drawing.Size(637, 91);
            this.labelAfterFirstCoise.TabIndex = 14;
            this.labelAfterFirstCoise.Text = resources.GetString("labelAfterFirstCoise.Text");
            this.labelAfterFirstCoise.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSecondAfterFirstChoise
            // 
            this.labelSecondAfterFirstChoise.AllowDrop = true;
            this.labelSecondAfterFirstChoise.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labelSecondAfterFirstChoise.BackColor = System.Drawing.Color.LightSeaGreen;
            this.labelSecondAfterFirstChoise.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSecondAfterFirstChoise.Location = new System.Drawing.Point(431, 626);
            this.labelSecondAfterFirstChoise.Name = "labelSecondAfterFirstChoise";
            this.labelSecondAfterFirstChoise.Size = new System.Drawing.Size(637, 91);
            this.labelSecondAfterFirstChoise.TabIndex = 15;
            this.labelSecondAfterFirstChoise.Text = resources.GetString("labelSecondAfterFirstChoise.Text");
            this.labelSecondAfterFirstChoise.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelThirdAfterFirstChoise
            // 
            this.labelThirdAfterFirstChoise.AllowDrop = true;
            this.labelThirdAfterFirstChoise.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labelThirdAfterFirstChoise.BackColor = System.Drawing.Color.LightSeaGreen;
            this.labelThirdAfterFirstChoise.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelThirdAfterFirstChoise.Location = new System.Drawing.Point(431, 626);
            this.labelThirdAfterFirstChoise.Name = "labelThirdAfterFirstChoise";
            this.labelThirdAfterFirstChoise.Size = new System.Drawing.Size(637, 91);
            this.labelThirdAfterFirstChoise.TabIndex = 16;
            this.labelThirdAfterFirstChoise.Text = "Дівчинка*: Ні бляха старенька, тобі яке діло?                                    " +
    "                                        ";
            this.labelThirdAfterFirstChoise.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonGood2
            // 
            this.buttonGood2.Location = new System.Drawing.Point(476, 457);
            this.buttonGood2.Name = "buttonGood2";
            this.buttonGood2.Size = new System.Drawing.Size(110, 38);
            this.buttonGood2.TabIndex = 17;
            this.buttonGood2.Text = "Огризнутися";
            this.buttonGood2.UseVisualStyleBackColor = true;
            this.buttonGood2.Click += new System.EventHandler(this.buttonGood2_Click);
            // 
            // buttonGood3
            // 
            this.buttonGood3.Location = new System.Drawing.Point(936, 458);
            this.buttonGood3.Name = "buttonGood3";
            this.buttonGood3.Size = new System.Drawing.Size(104, 36);
            this.buttonGood3.TabIndex = 18;
            this.buttonGood3.Text = "Промовчати";
            this.buttonGood3.UseVisualStyleBackColor = true;
            this.buttonGood3.Click += new System.EventHandler(this.buttonGood3_Click);
            // 
            // labelArgue
            // 
            this.labelArgue.AllowDrop = true;
            this.labelArgue.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labelArgue.BackColor = System.Drawing.Color.LightSeaGreen;
            this.labelArgue.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelArgue.Location = new System.Drawing.Point(431, 627);
            this.labelArgue.Name = "labelArgue";
            this.labelArgue.Size = new System.Drawing.Size(637, 91);
            this.labelArgue.TabIndex = 19;
            this.labelArgue.Text = "Та пішла ти";
            this.labelArgue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelIgnore
            // 
            this.labelIgnore.AllowDrop = true;
            this.labelIgnore.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labelIgnore.BackColor = System.Drawing.Color.LightSeaGreen;
            this.labelIgnore.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelIgnore.Location = new System.Drawing.Point(431, 626);
            this.labelIgnore.Name = "labelIgnore";
            this.labelIgnore.Size = new System.Drawing.Size(637, 91);
            this.labelIgnore.TabIndex = 20;
            this.labelIgnore.Text = "Ладно всеодно,подумав про себе і пішов гризти граніт науки";
            this.labelIgnore.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonCloseProgram
            // 
            this.buttonCloseProgram.Location = new System.Drawing.Point(12, 12);
            this.buttonCloseProgram.Name = "buttonCloseProgram";
            this.buttonCloseProgram.Size = new System.Drawing.Size(99, 35);
            this.buttonCloseProgram.TabIndex = 21;
            this.buttonCloseProgram.Text = "Закрити програму";
            this.buttonCloseProgram.UseVisualStyleBackColor = true;
            this.buttonCloseProgram.Click += new System.EventHandler(this.buttonCloseProgram_Click);
            // 
            // buttonGoToManeMenu
            // 
            this.buttonGoToManeMenu.Location = new System.Drawing.Point(697, 374);
            this.buttonGoToManeMenu.Name = "buttonGoToManeMenu";
            this.buttonGoToManeMenu.Size = new System.Drawing.Size(131, 50);
            this.buttonGoToManeMenu.TabIndex = 22;
            this.buttonGoToManeMenu.Text = "Вийти в головне меню,бо ви пройшли цю лінію сюжету";
            this.buttonGoToManeMenu.UseVisualStyleBackColor = true;
            this.buttonGoToManeMenu.Click += new System.EventHandler(this.buttonGoToManeMenu_Click);
            // 
            // labelForBadone
            // 
            this.labelForBadone.AllowDrop = true;
            this.labelForBadone.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labelForBadone.BackColor = System.Drawing.Color.LightSeaGreen;
            this.labelForBadone.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelForBadone.Location = new System.Drawing.Point(431, 626);
            this.labelForBadone.Name = "labelForBadone";
            this.labelForBadone.Size = new System.Drawing.Size(637, 91);
            this.labelForBadone.TabIndex = 23;
            this.labelForBadone.Text = "Тяжко прокидатися стало чогось,чого б це поробити";
            this.labelForBadone.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelBadTwo
            // 
            this.labelBadTwo.AllowDrop = true;
            this.labelBadTwo.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labelBadTwo.BackColor = System.Drawing.Color.LightSeaGreen;
            this.labelBadTwo.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelBadTwo.Location = new System.Drawing.Point(431, 626);
            this.labelBadTwo.Name = "labelBadTwo";
            this.labelBadTwo.Size = new System.Drawing.Size(637, 91);
            this.labelBadTwo.TabIndex = 24;
            this.labelBadTwo.Text = "Може то в парк піти чи вдома посидіти кінцо глянути..хммм";
            this.labelBadTwo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonStayHome
            // 
            this.buttonStayHome.Location = new System.Drawing.Point(476, 458);
            this.buttonStayHome.Name = "buttonStayHome";
            this.buttonStayHome.Size = new System.Drawing.Size(110, 38);
            this.buttonStayHome.TabIndex = 25;
            this.buttonStayHome.Text = "Залишитися вдома";
            this.buttonStayHome.UseVisualStyleBackColor = true;
            this.buttonStayHome.Click += new System.EventHandler(this.buttonStayHome_Click);
            // 
            // buttonGoSomewhere
            // 
            this.buttonGoSomewhere.Location = new System.Drawing.Point(934, 456);
            this.buttonGoSomewhere.Name = "buttonGoSomewhere";
            this.buttonGoSomewhere.Size = new System.Drawing.Size(110, 38);
            this.buttonGoSomewhere.TabIndex = 26;
            this.buttonGoSomewhere.Text = "Піти кудись";
            this.buttonGoSomewhere.UseVisualStyleBackColor = true;
            this.buttonGoSomewhere.Click += new System.EventHandler(this.buttonGoSomewhere_Click);
            // 
            // labelGoSomewhere
            // 
            this.labelGoSomewhere.AllowDrop = true;
            this.labelGoSomewhere.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labelGoSomewhere.BackColor = System.Drawing.Color.LightSeaGreen;
            this.labelGoSomewhere.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelGoSomewhere.Location = new System.Drawing.Point(431, 627);
            this.labelGoSomewhere.Name = "labelGoSomewhere";
            this.labelGoSomewhere.Size = new System.Drawing.Size(637, 91);
            this.labelGoSomewhere.TabIndex = 27;
            this.labelGoSomewhere.Text = "Погодка класна,прогуляюсь мабть";
            this.labelGoSomewhere.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStayHome
            // 
            this.labelStayHome.AllowDrop = true;
            this.labelStayHome.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labelStayHome.BackColor = System.Drawing.Color.LightSeaGreen;
            this.labelStayHome.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelStayHome.Location = new System.Drawing.Point(431, 626);
            this.labelStayHome.Name = "labelStayHome";
            this.labelStayHome.Size = new System.Drawing.Size(637, 91);
            this.labelStayHome.TabIndex = 28;
            this.labelStayHome.Text = "Нєєє,краще вдома побуду";
            this.labelStayHome.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1498, 777);
            this.Controls.Add(this.labelStayHome);
            this.Controls.Add(this.labelGoSomewhere);
            this.Controls.Add(this.buttonGoSomewhere);
            this.Controls.Add(this.buttonStayHome);
            this.Controls.Add(this.labelBadTwo);
            this.Controls.Add(this.labelForBadone);
            this.Controls.Add(this.buttonGoToManeMenu);
            this.Controls.Add(this.buttonCloseProgram);
            this.Controls.Add(this.labelIgnore);
            this.Controls.Add(this.labelArgue);
            this.Controls.Add(this.buttonGood3);
            this.Controls.Add(this.buttonGood2);
            this.Controls.Add(this.labelThirdAfterFirstChoise);
            this.Controls.Add(this.labelSecondAfterFirstChoise);
            this.Controls.Add(this.labelAfterFirstCoise);
            this.Controls.Add(this.labelbeforeFirstChoise);
            this.Controls.Add(this.buttonGoStaight);
            this.Controls.Add(this.buttonGoAnotherWay);
            this.Controls.Add(this.labelfirstslide);
            this.Controls.Add(this.buttonBackSlide);
            this.Controls.Add(this.buttonNextSlide);
            this.Controls.Add(this.labelGreetings);
            this.Controls.Add(this.buttonInfo);
            this.Controls.Add(this.buttonBeginGame);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1514, 816);
            this.MinimumSize = new System.Drawing.Size(1514, 816);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "WinterStory";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonBeginGame;
        private System.Windows.Forms.Button buttonInfo;
        private System.Windows.Forms.Label labelGreetings;
        private System.Windows.Forms.Button buttonNextSlide;
        private System.Windows.Forms.Button buttonBackSlide;
        private System.Windows.Forms.Label labelfirstslide;
        private System.Windows.Forms.Button buttonGoAnotherWay;
        private System.Windows.Forms.Button buttonGoStaight;
        private System.Windows.Forms.Label labelbeforeFirstChoise;
        private System.Windows.Forms.Label labelAfterFirstCoise;
        private System.Windows.Forms.Label labelSecondAfterFirstChoise;
        private System.Windows.Forms.Label labelThirdAfterFirstChoise;
        private System.Windows.Forms.Button buttonGood2;
        private System.Windows.Forms.Button buttonGood3;
        private System.Windows.Forms.Label labelArgue;
        private System.Windows.Forms.Label labelIgnore;
        private System.Windows.Forms.Button buttonCloseProgram;
        private System.Windows.Forms.Button buttonGoToManeMenu;
        private System.Windows.Forms.Label labelForBadone;
        private System.Windows.Forms.Label labelBadTwo;
        private System.Windows.Forms.Button buttonStayHome;
        private System.Windows.Forms.Button buttonGoSomewhere;
        private System.Windows.Forms.Label labelGoSomewhere;
        private System.Windows.Forms.Label labelStayHome;
    }
}

